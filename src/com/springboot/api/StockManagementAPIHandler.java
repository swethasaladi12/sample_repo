package com.springboot.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class StockManagementAPIHandler {

	private  static HashMap<Integer,StockObject> stockStore = new HashMap<Integer, StockObject>();

	@RequestMapping(value = "/products/{filter_column}/{filter_value}", method = RequestMethod.GET)
	public ResponseEntity<Object> filter(@PathVariable("filter_column") String filterColumn,@PathVariable("filter_value") String filterValue) { 
		List<StockObject> allStockObject = new ArrayList<StockObject>(stockStore.values());
		Predicate<StockObject> filterWithColValue = null;

		switch (filterColumn) {
		case "STOCKNAME":
			filterWithColValue =  e -> e.getStockName() == filterValue;
			break;
		case "PRODUCTPRICE":
			filterWithColValue =  e -> e.getProductPrice() == Integer.parseInt(filterValue);
			break;
		case "PURCHASEDATE":
			filterWithColValue =  e -> e.getPurchaseDate().toString() == filterValue;
			break;
		}
		
		List<StockObject> res = new ArrayList<StockObject>();
		allStockObject.stream().filter(filterWithColValue).forEach(st->res.add(st));;
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") int stockNumber) { 
		stockStore.remove(stockNumber);
		return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct( @RequestBody StockObject stockEntry) { 
		if(stockEntry.getQuantity()==0)
		{
			stockStore.remove(stockEntry.getStockNumber());
		}else {
			stockStore.put(stockEntry.getStockNumber(), stockEntry);
		}
		return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<Object> createProduct(@RequestBody StockObject stockEntry) {
		if(stockStore.get(stockEntry.getStockNumber())==null) {
			stockStore.put(stockEntry.getStockNumber(), stockEntry);
			return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
		}
		return new ResponseEntity<>("Product is created successfully", HttpStatus.ALREADY_REPORTED);
	}

	/**
	 * return all the stocks
	 * @return
	 */
	@RequestMapping(value = "/products")
	public ResponseEntity<Object> getProduct() {
		return new ResponseEntity<>(stockStore.values(), HttpStatus.OK);
	}
}